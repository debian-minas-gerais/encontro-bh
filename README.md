# I Encontro da Comunidade de Software Livre de BH e região

Não é necessário fazer inscrição. É só chegar e partiicipar :-)

## Data e horário

- 22 de outubro de 2022 (sábado)
- 9:00h às 17:10h

## Local

- **Escola Municipal Aurélio Pires**
- Sala 9
- Endereço:  Rua Barrinha, 171 - Indaiá - Belo Horizonte
- Próximo da Unifenas e da UFMG
- [Mapa](https://www.openstreetmap.org/search?query=escola%20municipal%20aur%C3%A9lio%20pires#map=19/-19.86530/-43.95238)

## Programação

-  9:00 - Chegada
- 10:00 - O que é Software Livre, Código Aberto, formatos abertos e Linux - para quem é de fora da "torre de silício" - Francisco @laranjatomate
- 11:00 - Educação (com software livre) é bom e eu gosto! - Frederico Guimarães
- 12:00 - intervalo/almoço
- 14:00 - O que é Debian - Paulo Santana
- 15:00 - KDE: do ambiente desktop à comunidade - Frederico Guimarães
- 16:00 - Discussão/Mesa: como está a comunidade de SL em BH e em MG?
- 17:00 - encerramento

## Organização

[Debian Minas Gerais](https://debian-minas-gerais.gitlab.io/site/)
