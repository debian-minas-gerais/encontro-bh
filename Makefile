all: public/index.html public/style.css

public/index.html: public

public:
	mkdir -p public

public/index.html: README.md style.css
	pandoc \
		--metadata pagetitle='I Encontro da Comunidade de Software Livre de BH e região' \
		--standalone \
		--from=markdown \
		--to=html5 \
		--css=style.css \
		--output=$@ \
		README.md

public/style.css: style.css
	cp $< $@

clean:
	$(RM) -r public
